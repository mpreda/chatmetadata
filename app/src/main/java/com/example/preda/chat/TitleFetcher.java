package com.example.preda.chat;

import android.support.annotation.Nullable;
import android.text.Html;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A {@link TitleResolver} that does an HTTP request to read the HTML page, and scans an initial
 * segment of the page for a <title>Title</title> regexp. The title is returned if found, or null
 * otherwise.
 *
 * This implementation attempts to limit the amount of data downloaded in two ways: by doing "lazy
 * download", progresivelly downloading more as necessary while the title has not yet been found
 * (vs. "eager downloading"). And by imposing a size limit on the initial segment of the page that
 * is scanned for the title (based on the expectation that the title is located towards the
 * beginning of the document).
 *
 * Note: the title scanning could be more efficient than by using {@link Scanner}, for example by
 * not keeping in memory the prefix of the page that is known to not contain any part of the title,
 * but such an involved implementation might be overkill in the context.
 *
 * The charset (needed for correct conversion bytes-to-chars) is read from the Content-Type.
 * The MIME type is used to limit the scan to text types only (to avoid searching for title in
 * images, for example).
 */
public class TitleFetcher implements TitleResolver {

    // Default charset used when Content-Type does not specify a charset.
    private static final String DEFAULT_CHARSET = "UTF-8";

    // Look for title only in the first HEADER_LOOKUP_CHARS characters of the HTML document.
    // This is to protect against downloading huge documents with no title.
    private static final int HEADER_LOOKUP_CHARS = 32 * 1024;

    private static final Pattern charsetPattern =
            Pattern.compile("(?i)\\bcharset=\\s*\"?([^\\s;\"]*)");

    private static final Pattern titlePattern = Pattern.compile("<title>[^<]*</title>");

    private static final char ELLIPSIS = '\u2026';

    private final int maxTitleChars;

    public TitleFetcher(int maxTitleChars) {
        if (maxTitleChars < 1) {
            throw new IllegalArgumentException("Title length limit must be >= 1");
        }
        this.maxTitleChars = maxTitleChars;
    }

    /**
     * Returns the charset contained in a Content-Type spec, or DEFAULT_CHARSET if not found.
     */
    private static String parseCharset(String contentType) {
        Matcher m = charsetPattern.matcher(contentType);
        return m.find() ? m.group(1).trim().toUpperCase() : DEFAULT_CHARSET;
    }

    /**
     * Enforce maximum length by truncating and appending ellipsis at the end.
     */
    private String ellipsize(String title) {
        return (title.length() > maxTitleChars)
                ? title.substring(0, maxTitleChars - 1) + ELLIPSIS
                : title;
    }

    /**
     * Reverse HTML entity escaping; e.g. &quot; becomes "
     */
    private String unescape(String s) {
        return Html.fromHtml(s).toString();
    }

    @Override
    public @Nullable String getTitle(String url) throws IOException {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            String contentType = connection.getContentType();
            if (contentType == null) {
                return null;  // Expect HTML to have a Content-Type.
            }
            contentType = contentType.trim().toLowerCase();
            if (!contentType.toLowerCase().startsWith("text/")) {
                // Not a text MIME type, in particular not HTML.
                return null;
            }
            String charset = parseCharset(contentType);
            Scanner scanner =
                    new Scanner(new BufferedInputStream(connection.getInputStream()), charset);
            String title = scanner.findWithinHorizon(titlePattern, HEADER_LOOKUP_CHARS);
            scanner.close();
            if (title == null) {
                return null;
            }
            title = title.substring("<title>".length(), title.length() - "</title>".length());
            return ellipsize(unescape(title));
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
