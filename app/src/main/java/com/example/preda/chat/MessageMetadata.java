package com.example.preda.chat;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.util.Patterns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  Class used to represent metadata information about a chat message. For now, it only extracts
 *  metadata information from a chat message and formats it into a JSON-syntax string.
 */
public class MessageMetadata {
    private static final String TAG = "MessageMetadata";
    private static final String URL_KEY = "url";
    private static final String TITLE_KEY = "title";
    private static final String LINKS_KEY = "links";
    private static final String MENTIONS_KEY = "mentions";
    private static final String EMOTICONS_KEY = "emoticons";

    // Maximum title length in chars. Can be exposed as an argument if needed.
    private static final int MAX_TITLE_CHARS = 50;

    private static final Pattern mentionPattern = Pattern.compile("@(\\w+)");
    private static final Pattern emoticonPattern = Pattern.compile("\\((\\p{Alnum}+)\\)");
    private static final Pattern escapedSlashPattern = Pattern.compile("\\\\/");

    private final List<String> mentions;
    private final List<String> emoticons;
    private final List<String> urls;

    public MessageMetadata(String message) {
        mentions = getOccurences(mentionPattern, message, 1);
        emoticons = getOccurences(emoticonPattern, message, 1);
        urls = getUrls(message);
    }

    /**
     * Filter URLs with one of the protocols with can process, i.e. HTTP or HTTPS.
     */
    private static List<String> getUrls(String message) {
        List<String> rawUrls = getOccurences(Patterns.WEB_URL, message, 0);
        List<String> filteredUrls = new ArrayList<>();
        for (String url : rawUrls) {
            String lowPrefix = url.substring(0, "https://".length()).toLowerCase();
            if (lowPrefix.startsWith("http://") || lowPrefix.startsWith("https://")) {
                filteredUrls.add(url);
            }
        }
        return filteredUrls;
    }

    /**
     * Returns all the occurrences of pattern in a string.
     *
     * @param pattern the pattern to be extracted.
     * @param message the string scanned.
     * @param group the group that is returned. Use 0 to get the whole match.
     * @return a list of matches, according to pattern and group.
     */
    private static List<String> getOccurences(Pattern pattern, String message, int group) {
        Matcher m = pattern.matcher(message);
        List<String> occurences = new ArrayList<>();
        while (m.find()) {
            occurences.add(m.group(group));
        }
        return occurences;
    }

    private JSONArray getLinks(TitleResolver titleResolver) throws JSONException {
        List<JSONObject> links = new ArrayList<>();
        for (String url : urls) {
            JSONObject urlInfo = new JSONObject();
            urlInfo.put(URL_KEY, url);
            String title = null;
            try {
                title = titleResolver.getTitle(url);
            } catch (IOException e) {
                Log.d(TAG, "Title resolve error", e);
            }
            if (title != null) {
                urlInfo.put(TITLE_KEY, title);
            }
            links.add(urlInfo);
        }
        return new JSONArray(links);
    }

    private JSONObject getJson(TitleResolver titleResolver) throws JSONException {
        JSONObject json = new JSONObject();
        if (!mentions.isEmpty()) {
            json.put(MENTIONS_KEY, new JSONArray(mentions));
        }
        if (!emoticons.isEmpty()) {
            json.put(EMOTICONS_KEY, new JSONArray(emoticons));
        }
        if (!urls.isEmpty()) {
            json.put(LINKS_KEY, getLinks(titleResolver));
        }
        return json;
    }

    // Undoes JSONObject.toString()'s escape of slash '/' to "\\/".
    private static String unescapeSlashes(String s) {
        Matcher m = escapedSlashPattern.matcher(s);
        return m.replaceAll("/");
    }

    /**
     * Return a string with this metadata formatted in JSON syntax.
     *
     * @param titleResolver used for resolving the title of URLs.
     * @return metadata string in JSON syntax as per the task description.
     */
    private String getMetadataString(TitleResolver titleResolver, boolean multiline) {
        try {
            JSONObject json = getJson(titleResolver);
            return unescapeSlashes(multiline ? json.toString(0) : json.toString());
        } catch (JSONException e) {
            // We expect JSONException never be raised for our usage. We don't want to swallow
            // it though, so we turn it into an assertion failure instead.
            throw new AssertionError(e);
        }
    }

    /**
     * Returns a metadata string as per the task description, with the observation that any
     * title that can't be resolved (e.g. because title not found, or because HTTP connection can't
     * be established) results in a links entry with only "url" but without "title".
     *
     * This method does blocking network I/O and thus must not be invoked on the UI thread!
     *
     * @return metadata string in JSON syntax as per the task description.
     */
    public String toStringBlocking() {
        return getMetadataString(new TitleFetcher(MAX_TITLE_CHARS), true);
    }

    /**
     * This method is similar to {@link #toStringBlocking()} but does no blocking
     * operations and can be invoked on the UI thread; it does not resolve titles though.
     */
    // A variant would be to use a caching-only TitleResolver, which would still resolve a set of
    // cached titles in non-blocking fashion; also useful when offline.
    @Override
    public String toString() {
        return getMetadataString(new TitleResolver() {
            @Nullable
            @Override
            public String getTitle(String url) throws IOException {
                return null;
            }
        }, true);
    }

    /**
     * Returns whether the message contains any URLs.
     *
     * Note: a message without URLs does not block for fetching titles.
     */
    public boolean hasUrls() {
        return !urls.isEmpty();
    }

    @VisibleForTesting
    String toStringSingleLine() {
        return getMetadataString(new TitleFetcher(MAX_TITLE_CHARS), false);
    }
}
