package com.example.preda.chat;

import android.support.annotation.Nullable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;

/**
 * TitleResolver allows to resolve the title of an URL in different ways, such as fetching from
 * the network or using a local cache when the network is not available.
 */
public interface TitleResolver {
    @Nullable String getTitle(String url) throws IOException;
}
