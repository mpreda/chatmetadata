package com.example.preda.chat;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class DemoActivity extends AppCompatActivity {
    private static final String[] EXAMPLES = {
            "@chris you around?",
            "Good morning! (megusta) (coffee)",
            "Olympics are starting soon; http://www.nbcolympics.com",
            "hi @bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",
            "see http:www.yahoo.com http://twitter.com/"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);
        final TextView input = (TextView) findViewById(R.id.input);
        final TextView output = (TextView) findViewById(R.id.output);
        final View loading = findViewById(R.id.loading);
        if (input == null || output == null || loading == null) {
            Log.e("ChatDemo", "Could not find views");
            return;
        }

        String message = EXAMPLES[3];
        MessageMetadata metadata = new MessageMetadata(message);
        input.setText(message);

        // Set the output early with all the information that's available non-blocking
        // -- basically everything but the eventual titles.
        output.setText(metadata.toString());

        if (metadata.hasUrls()) {
            loading.setVisibility(View.VISIBLE);
            // Asynchronously fetch the titles, and update output when done. Display progress
            // indicator during fetching.
            new AsyncTask<MessageMetadata, Void, String>() {
                @Override
                protected String doInBackground(MessageMetadata... params) {
                    return params[0].toStringBlocking();
                }

                @Override
                protected void onPostExecute(String result) {
                    output.setText(result);
                    loading.setVisibility(View.GONE);
                }
            }.execute(metadata);
        }
    }
}
