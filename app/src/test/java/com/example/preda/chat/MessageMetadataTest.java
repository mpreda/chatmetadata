package com.example.preda.chat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk=19)
public class MessageMetadataTest {
    static String[] input = {
            "",
            "http:yahoo.com",
            "rtsp://foo.com",
            "@chris you around?",
            "Good morning! (megusta) (coffee)",
            "Olympics are starting soon; http://www.nbcolympics.com",
            "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",
    };

    static String[] output = {
            "{}",
            "{}",
            "{}",
            "{\"mentions\":[\"chris\"]}",
            "{\"emoticons\":[\"megusta\",\"coffee\"]}",
            "{\"links\":[{\"title\":\"2016 Rio Olympic Games | NBC Olympics\",\"url\":\"http://www.nbcolympics.com\"}]}",
            "{\"emoticons\":[\"success\"],\"mentions\":[\"bob\",\"john\"],\"links\":[{\"title\":\"Justin Dorfman on Twitter: \\\"nice @littlebigdetail…\",\"url\":\"https://twitter.com/jdorfman/status/430511497475670016\"}]}",
    };

    @Test
    public void test() {
        for (int i = 0; i < input.length; ++i) {
            assertEquals(output[i], new MessageMetadata(input[i]).toStringSingleLine());
        }
    }
}